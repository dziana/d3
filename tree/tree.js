const duration = 500;
const levels = 5;
let amount = 0;
let treeData =
    {
        "name": "root",
        "children": []
    };

function createTreeData(level, node) {

    for (let i = 0; i <= getRandomNumber(0, 3); i++) {
        node.children = [...node.children, {name: 'Level' + level, children: []}];
        amount++;
    }
    if (level > levels)
        return;
    node.children.forEach(
        e => createTreeData(level + 1, e)
    )
}

createTreeData(0, treeData);
const margin = {top: 40, right: 120, bottom: 20, left: 120};
const width = amount * 17 - margin.right - margin.left;
const height = levels * 80 - margin.top - margin.bottom;

const svg = d3.select("body")
    .append("svg")
    .attr("height", height + margin.top + margin.bottom)
    .attr("width", width + margin.right + margin.left)
    .append('g')
    .attr("transform", `translate(${margin.left},${margin.top})`)
    .attr("height", height)
    .attr("width", width);


const root = d3.hierarchy(treeData);
root.descendants().forEach((d, i) => {
    d.id = i;
    d._children = d.children;
    // if (d.depth >= 2) d.children = null
});
update(root);


function update(source) {
    let strW = d3.scaleLinear()
        .domain([0, levels])
        .range([10, 1]);
    const {x, y} = source;
    d3.select("svg")
        .transition()
        .duration(duration)
        .attr("height", height + margin.top + margin.bottom)
        .attr("width", width + margin.right + margin.left);


    const tree = d3.tree().size([width, height]);
    tree(root);

    const nodes = root.descendants();
    const links = root.links();
    const updateLinks = svg.selectAll('path')
        .data(links, d => d.target.id);

    updateLinks
        .enter()
        .append('path')
        .classed('link', true)
        .attr('stroke-width', d => {
            return strW(d.source.depth);
        })
        .attr("d", d3.linkVertical()
            .y(y ? height - y : height - source.y)
            .x(x ? x : source.x))
        .merge(updateLinks)
        .transition()
        .duration(duration)
        .attr("d", d3.linkVertical()
            .y(d => height - d.y)
            .x(d => d.x));

    updateLinks.exit()
        .transition()
        .duration(duration)
        .attr("d", d3.linkVertical()
            .y(height - source.y)
            .x(source.x))
        .remove();

    const updateNodes = svg.selectAll('circle')
        .data(nodes, d => d.id);

    updateNodes
        .enter()
        .append('circle')
        .classed('node', true)
        .attr("stroke", d => d._children ? "green" : "grey")
        .attr('r', 7)
        .attr('cy', y ? height - y : height - source.y)
        .attr('cx', x ? x : source.x)
        .merge(updateNodes)
        .on("click", (d) => {
            d.children = d.children ? null : d._children;
            return update(d)
        })
        .transition()
        .duration(duration)
        .attr('cy', d => height - d.y)
        .attr('cx', d => d.x);


    updateNodes.exit()
        .transition()
        .duration(duration)
        .attr('cy', height - source.y)
        .attr('cx', source.x)
        .remove()

    //
    // updateLinks
    //     .enter()
    //     .append('path')
    //     .classed('link', true)
    //     .attr("d", d3.linkVertical()
    //         .y(y ? y : height - source.y)
    //         .x(x ? x : source.x))
    //     .merge(updateLinks)
    //     .transition()
    //     .duration(duration)
    //     .attr("d", d3.linkVertical()
    //         .x(d => d.x)
    //         .y(d => height - d.y));
    //
    // updateLinks.exit()
    //     .transition()
    //     .duration(duration)
    //     .attr("d", d3.linkVertical()
    //         .y(height - source.y)
    //         .x(source.x))
    //     .remove();
    //
    // const updateNodes = svg.selectAll('circle')
    //     .data(nodes, d => d.id);
    //
    // updateNodes
    //     .enter()
    //     .append('circle')
    //     .classed('node', true)
    //     .attr("stroke", d => d._children ? "green" : "grey")
    //     .attr('r', 7)
    //     .attr('cx', x ? x : source.x)
    //     .attr('cy', y ? height - y : height - source.y)
    //     .on("click", (d) => {
    //         d.children = d.children ? null : d._children;
    //         return update(d)
    //     })
    //     .transition()
    //     .duration(duration)
    //     .attr('cy', d => height - d.y)
    //     .attr('cx', d => d.x);
    //
    // updateNodes.exit()
    //     .transition()
    //     .duration(duration)
    //     .attr('cy', height - source.y)
    //     .attr('cx', source.x)
    //     .remove()
}

function getRandomNumber(start, end) {
    return (Math.floor((Math.random() * (end - start))) + start);
}