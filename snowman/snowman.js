const width = window.innerWidth - 10;
const height = window.innerHeight - 5;
const wordsArray = ['Snow', 'Winter', 'Happy New Year', 'Christmas', 'Snowballs', 'Snowboarding', 'Candles', 'Gifts', 'Socks', 'Christmas tree', 'Santa Claus', 'Olivie'];
let myWords = [];
for (let i = 0; i < 7; i++) {
    wordsArray.forEach(e => {
        myWords = [...myWords, {
            word: e,
            size: getRandomNumber(2, 6) * 5
        }]
    })
}


const svg = d3.select("#snowman")
    .style("background-color", "#000000")
    .append('svg')
    .attr("width", width)
    .attr("height", height);

const svgText = svg
    .append('g');

const svgSnow = svg
    .append('g');

const svgBody = svg
    .append('g');


let layout = d3.layout.cloud()
    .size([width, height - 600])
    .words(myWords.map(function (d) {
        return {text: d.word, size: d.size};
    }))
    .padding(5)
    .rotate(function () {
        return ~~(Math.random() * 2) * 90;
    })
    .fontSize(function (d) {
        return d.size;
    })
    .on("end", draw);

function draw(words) {
    let offsetY = layout.size()[1] / 2 + 600;
    svgText
        .attr("transform", "translate(" + (layout.size()[0] / 2) + "," + offsetY + ")")
        .selectAll("text")
        .data(words)
        .enter().append("text")
        .style("font-size", function (d) {
            return d.size;
        })
        .style("fill", "white")
        .attr("text-anchor", "middle")
        .style("font-family", "Impact")
        .attr("transform", function (d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
        })
        .text(function (d) {
            return d.text;
        });
}


function drawSnowmanBody() {
    d3.timer(addSnow, 100);
    const rBody = 60;
    let alpha = getRandomNumber(25, rBody);
    addSpiro(250, rBody, alpha, width / 2, 600);
    addSpiro(150, rBody, alpha, width / 2, 300);
    addSpiro(100, rBody, alpha, width / 2, 100);
    const rArms = 10;
    let alpha2 = getRandomNumber(5, rArms);
    addSpiro(40, rArms, alpha2, width / 2 + 150, 300 - 40);
    addSpiro(40, rArms, alpha2, width / 2 - 150, 300 - 40);
}

function addSnow() {
    for (let i = 0; i < 2; i++) {
        let sn = svgSnow
            .append('circle')
            .attr('r', getRandomNumber(1, 5))
            .attr('fill', 'white')
            .attr('cx', getRandomNumber(0, width))
            .attr('cy', -1)
            .transition()
            .duration(10000)
            .attr('cy', height)
            .remove();
    }
}

var line = d3.line()
    .x(function (d) {
        return d.x;
    })
    .y(function (d) {
        return d.y;
    });

function addSpiro(R, r, alpha, x, y) {
    let path = svgBody.append("path")
        .attr("class", "spirograph")
        .attr("d", line(plotSpiroGraph(R, r, alpha, x, y)))
        .style("stroke", "#00AC93");
    const totalLength = path.node().getTotalLength();
    const dashArray = totalLength + " " + totalLength;
    path
        .attr("stroke-dasharray", dashArray)
        .attr("stroke-dashoffset", totalLength)
        .transition().duration(1000).ease(d3.easeLinear)
        .attr("stroke-dashoffset", 0);

}

function plotSpiroGraph(R, r, alpha, xCenter, yCenter) {
    const l = alpha / r;
    const k = r / R;

    let lineData = [];
    for (var theta = 1; theta <= 20000; theta += 1) {
        let t = ((Math.PI / 180) * theta);
        let ang = ((l - k) / k) * t;
        let x = R * ((1 - k) * Math.cos(t) + ((l * k) * Math.cos(ang))) + xCenter;
        let y = R * ((1 - k) * Math.sin(t) - ((l * k) * Math.sin(ang))) + yCenter;

        lineData.push({x: x, y: y});
    }
    console.log(R, r, alpha);
    return lineData;
}

function getRandomNumber(start, end) {
    return (Math.floor((Math.random() * (end - start))) + start);
}


layout.start();
drawSnowmanBody();