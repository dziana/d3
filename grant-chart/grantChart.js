const data = [
    {
        id: 1,
        startDate: new Date(2010, 0, 1),
        endDate: new Date(2014, 0, 1),
        executedDate: null,
        name: 'task 1'
    },
    {
        id: 2,
        startDate: new Date(2010, 0, 1),
        endDate: new Date(2018, 0, 1),
        executedDate: new Date(2014, 0, 1),
        name: 'task 2'
    },
    {
        id: 3,
        startDate: new Date(2012, 0, 1),
        endDate: new Date(2014, 0, 1),
        executedDate: new Date(2018, 0, 1),
        name: 'task 3'
    },
    {
        id: 4,
        startDate: new Date(2016, 0, 1),
        endDate: new Date(2024, 0, 1),
        executedDate: null,
        name: 'task 4'
    },
    {
        id: 5,
        startDate: new Date(2012, 0, 1),
        endDate: new Date(2015, 0, 1),
        executedDate: null,
        name: 'task 5'
    },
    {
        id: 6,
        startDate: new Date(2019, 0, 1),
        endDate: new Date(2020, 0, 1),
        executedDate: null,
        name: 'task 6'
    },
    {
        id: 7,
        startDate: new Date(2010, 0, 1),
        endDate: new Date(2025, 0, 1),
        executedDate: null,
        name: 'task 7'
    }
];

const keys = [
    {name: "in progress", color: '#AAFAB7'},
    {name: "overtime", color: '#FF5B42'},
    {name: "faster than planned", color: '#D4F9FC'},
    {name: "left", color: '#CED4CD'}
];

const legendLineSize = 20;
const chartInnerPadding = 20;
const chartMargin = {top: 10, right: 40, bottom: 30, left: 50},
    chartHeight = data.length * 50 - chartMargin.top - chartMargin.bottom;
let chartWidth = window.innerWidth/10*7 - chartMargin.left - chartMargin.right;

const svgGraph = d3.select("#diagramm");


function drawChart() {
    svgGraph.selectAll('svg').remove();
    let svgChart = svgGraph
        .append('svg')
        .attr("width", chartWidth + chartMargin.left + chartMargin.right)
        .attr("height", chartHeight + chartMargin.top + chartMargin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + chartMargin.left + "," + chartMargin.top + ")");
    const x = d3.scaleTime()
        .domain([d3.min(data, function(d) { return d.startDate; }), d3.max(data, function(d) {
            return Math.max(d.executedDate, d.endDate, new Date());
        })])
        .range([0, chartWidth - chartInnerPadding]);

    const y = d3.scalePoint()
        .domain(data.map(e => e.name))
        .range([chartHeight, 0])
        .padding([1]);


    svgChart
        .append('g')
        .attr("transform", "translate(" + chartInnerPadding + "," + chartHeight + ")")
        .attr("class", "x-axis")
        .call(d3.axisBottom(x))
        .attr("stroke", "white")
        .select(".domain").remove();

    svgChart
        .append('g')
        .attr("class", "y-axis")
        .call(d3.axisLeft(y))
        .attr("stroke", "white")
        .select(".domain").remove();

    svgChart.selectAll("g.x-axis g.tick")
        .append("line")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", 0)
        .attr("y2", -chartHeight)
        .attr('stroke', 'lightgray');

    svgChart.selectAll("g.y-axis g.tick")
        .append("line")
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", chartWidth)
        .attr("y2", 0)
        .attr('stroke', 'lightgray');

    let Tooltip = d3.select("#diagramm")
        .append("div")
        .style('display', 'none')
        .attr("class", "tooltip")
        .style("border", "solid")
        .style("border-width", "2px")
        .style("border-radius", "5px")
        .style("padding", "5px");

    let mouseover = function (d) {
        Tooltip
            .style('display', 'block');
        d3.select(this)
            .style("stroke", "white")
            .style('stroke-width', '1px')
    };
    let mousemove = function (d) {
        Tooltip
            .html("<strong>" + d.name +
                "</strong><br><strong>start date:</strong> " + d.startDate.toDateString() +
                "<br><strong>end date:</strong> " + d.endDate.toDateString() +
                "<br><strong>executed date:</strong> " + (d.executedDate ? d.executedDate.toDateString() : '--'))
            .attr("class", "tooltip")
            .style("left", (d3.mouse(this)[0] + 70) + "px")
            .style("top", (d3.mouse(this)[1]) + "px")
    };
    let mouseleave = function (d) {
        Tooltip
            .style("display", 'none');
        d3.select(this)
            .style("stroke", "none")
    };
    let svgChartArea = svgChart.append('g');

    svgChartArea
        .selectAll()
        .data(data)
        .enter()
        .append("rect")
        .attr("x", function (d) {
            return x(d.startDate)
        })
        .attr("y", function (d) {
            return y(d.name)
        })
        .attr("width", 0)
        .attr("height", function (d) {
            return y.step()
        })
        .attr('stroke', 'black')
        .attr("fill", '#aafab7')
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseleave", mouseleave)
        .transition()
        .ease(d3.easeLinear)
        .duration(200)
        .attr("width", function (d) {
            return d.executedDate ? Math.min(x(new Date()), x(d.endDate), x(d.executedDate)) - x(d.startDate) :
                Math.min(x(new Date()), x(d.endDate)) - x(d.startDate)
        });

    svgChartArea
        .selectAll()
        .data(data)
        .enter()
        .append("rect")
        .attr("x", function (d) {
            if (d.executedDate)
                return x(d.executedDate)
        })
        .attr("y", function (d) {
            return y(d.name)
        })
        .attr("width", 0)
        .attr("height", function (d) {
            return y.step()
        })
        .attr('stroke', 'black')
        .attr("fill", '#d4f9fc')
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseleave", mouseleave)
        .transition()
        .ease(d3.easeLinear)
        .duration(200)
        .delay(200)
        .attr("width", function (d) {
            if (d.executedDate && d.executedDate < d.endDate)
                return Math.min(x(d.endDate), x(new Date())) - x(d.executedDate)
        });

    svgChartArea
        .selectAll()
        .data(data)
        .enter()
        .append("rect")
        .attr("x", function (d) {
            if (x(d.endDate) < x(new Date()))
                return x(d.endDate)
        })
        .attr("y", function (d) {
            return y(d.name)
        })
        .attr("width", 0)
        .attr("height", function (d) {
            return y.step()
        })
        .attr('stroke', 'black')
        .attr("fill", '#ff5b42')
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseleave", mouseleave)
        .transition()
        .ease(d3.easeLinear)
        .duration(200)
        .delay(200)
        .attr("width", function (d) {
            if (!d.executedDate && d.endDate < new Date())
                return x(new Date()) - x(d.endDate);
            if (d.executedDate && d.executedDate > d.endDate)
                return x(d.executedDate) - x(d.endDate);
        });

    svgChartArea
        .selectAll()
        .data(data)
        .enter()
        .append("rect")
        .attr("x", function (d) {
            if (d.endDate > new Date())
                return x(new Date())
        })
        .attr("y", function (d) {
            return y(d.name)
        })
        .attr("width", 0)
        .attr("height", function (d) {
            return y.step()
        })
        .attr('stroke', 'black')
        .attr("fill", '#ced4cd')
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseleave", mouseleave)
        .transition()
        .ease(d3.easeLinear)
        .duration(200)
        .delay(200)
        .attr("width", function (d) {
            if (d.endDate > new Date())
                return x(d.endDate) - x(new Date())
        });

    svgChartArea.append("line")
        .attr('x1', x(new Date()))
        .attr('y1', 0)
        .attr('x2', x(new Date()))
        .attr('y2', 0)
        .style("stroke-dasharray", ("5, 5"))
        .style("stroke-width", '3px')
        .attr('stroke', 'red')
        .transition()
        .ease(d3.easeLinear)
        .duration(500)
        .delay(400)
        .attr('y2', chartHeight);
    drawLegent();
}

function drawLegent() {
    let svgLegend = svgGraph
        .append('svg')
        .attr('width', "30%")
        .attr('height', keys.length * (legendLineSize + 5));

    svgLegend.selectAll("dots")
        .data(keys)
        .enter()
        .append("rect")
        .attr("x", 0)
        .attr("y", function (d, i) {
            return i * (legendLineSize + 5)
        })
        .attr("width", legendLineSize)
        .attr("height", legendLineSize)
        .style("fill", function (d) {
            return d.color
        });

    svgLegend.selectAll("labels")
        .data(keys)
        .enter()
        .append("text")
        .attr("x", legendLineSize * 1.2)
        .attr("y", function (d, i) {
            return i * (legendLineSize + 5) + (legendLineSize / 2)
        })
        .style("fill", "white")
        .text(function (d) {
            return d.name
        })
        .attr("text-anchor", "left")
        .style("alignment-baseline", "middle");
}

drawChart();
window.onresize = function(event) {
    chartWidth = window.innerWidth/10*7 - chartMargin.left - chartMargin.right;
    drawChart();
};