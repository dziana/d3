const keys = ["<5", "5-12", "13-16", "17-21", "22-30", "31-45", "46-60", "60-70", ">70"]

function getRandomNumber(){
    return Math.floor(Math.random() * 50) + Math.floor(Math.random() * 10) * 0.1
}
function createRandomData(){
    const data = keys.map(item => {
        return {
            name: item,
            value: getRandomNumber()
        }
    });
    return drawChart(data);
}

document.addEventListener("DOMContentLoaded", () => createRandomData());
function drawChart(data){
    const width = 800;
    const height = 500;
    const radius = Math.min(width, height) / 2;

    const svg = d3.select("body").append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", `translate(${width / 2 }  ${height / 2})`);

    const arc = d3.arc()
        .innerRadius(radius / 2)
        .outerRadius(radius);

    const pie = d3.pie()
        .sort(null)
        .value(d => d.value);

    const color = d3.scaleLinear()
        .domain([0, pie(data).length])
        .range(['#E3E3FF', 'blue']);

    const g = svg.selectAll(".arc")
        .data(pie(data))
        .enter().append("g");

    g.append("path")
        .attr("fill", (d,i) => color(i))
        .attr("stroke", "white")
        .transition()
        .delay((d,i) => i * 200)
        .duration(200)
        .attrTween('d', (d) => {
            const i = d3.interpolate(d.startAngle, d.endAngle);
            return (t) => {
                d.endAngle = i(t);
                return arc(d)
            }
        });

    g.append("text")
        .attr("transform", d => `translate(${arc.centroid(d)})`)
        .attr("font-size", "12")
        .transition()
        .delay((d,i) => (i * 200) + 150)
        .text(d => d.data.name);

}